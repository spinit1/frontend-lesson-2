// кажется тут должен быть код...

// Подсказка №1: Содержимое тега textarea хранится в его свойстве value

// Подсказка №2: Не забывайте, что LocalStorage и SessionStorage могут хранить только строки в виде пар ключ/значение


function addNewComment()
{
    let newComment = document.querySelector('textarea').value

    if (!newComment) {
        return false
    }

    let oldComments = JSON.parse(localStorage.getItem('comments'))

    if (!oldComments) {
        oldComments = []
    }

    oldComments.push(newComment)

    localStorage.setItem('comments', JSON.stringify(oldComments))

    document.getElementById('add__comment').insertAdjacentHTML('beforeend',
        `
        <br>
        ${newComment}
        </br>
        `)
}
